package smtpplz;

import java.net.*;
import java.io.*;

public class thegoodstuff {
	static InputStream is;
	static OutputStream os;
	static Socket s;

	public thegoodstuff(String host, int port) throws UnknownHostException, IOException {
		s = new Socket(host, port);
		is = s.getInputStream();
		os = s.getOutputStream();
	}

	static void send_request(String request) throws IOException {
		os.write(request.getBytes());
		os.flush();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void print_response() throws IOException {
		byte allthechars[] = new byte[1024];
		int howmuch = is.read(allthechars);
		System.out.write(allthechars, 0, howmuch);
	}

	public void send_message(String from, String to, String subject, String text) throws IOException {

		from = "MAIL FROM: " + from + "\r\n";
		to = "RCPT TO: " + to + "\r\n";
		text = "DATA\r\n" + 
				"Subject: " + subject + "\r\n" +
				text +"\r\n" + 
				".\r\n";
		String chunk = "";
		String[] request_params = {from, to, text};
		for (int i = 0; i < request_params.length; i++) {
			chunk = request_params[i];
			send_request(chunk);
			print_response();
		}
	}

	public void close() throws IOException {
		send_request("QUIT\r\n");
		print_response();
		s.close();
	}

}
